package edu.ntnu.idatt2001.units;

import edu.ntnu.idatt2001.Unit;
import edu.ntnu.idatt2001.enums.Terrain;
import edu.ntnu.idatt2001.enums.UnitType;

/**
 * The type Ranged unit.
 */
public class RangedUnit extends Unit {

  private int timesAttacked = 0;

  /**
   * Instantiates a new Ranged unit.
   *
   * @param name   the name of this Ranged unit.
   * @param health the starting health of this Ranged unit.
   * @param armor  the armor of this Ranged unit. This value decides how much damage this unit takes from attacks.
   * @param attack the attack of this Ranged unit.
   * @throws IllegalArgumentException the illegal argument exception is thrown when name or health is less than or
   * equal to zero, or if name is blank.
   */
  public RangedUnit(String name, int health, int armor, int attack)
    throws IllegalArgumentException {
    super(name, health, armor, attack);
  }

  /**
   * Instantiates a new Ranged unit.
   *
   * @param name   the name of this Ranged unit.
   * @param health the starting health of this Ranged unit.
   * @throws IllegalArgumentException the illegal argument exception is thrown when name or health is less than or
   * equal to zero, or if name is blank.
   */
  public RangedUnit(String name, int health) throws IllegalArgumentException {
    super(name, health, 8, 15);
  }

  /**
   * Gets the unit type enum of this unit.
   *
   * @return the unit type enum of this unit (UnitType.RANGED)
   */
  @Override
  public UnitType getUnitType() {
    return UnitType.RANGED;
  }

  /**
   * This is the attack bonus that is used to calculate an attack.
   *
   * @return 3
   */
  @Override
  public int getAttackBonus(Terrain terrain) {
    if(terrain == Terrain.HILL) return 6;
    if(terrain == Terrain.FOREST) return 1;
    else return 3;
  }

  /**
   * This is the resistance bonus of the Ranged unit. Since Ranged units can attack from a distance, it has a
   * resistance bonus that gets smaller after each attack.
   *
   * @return resist bonus
   */
  @Override
  public int getResistBonus(Terrain terrain) {
    int startingResistance = 6;
    int finalResistance = 2;
    int resistanceDecrease = 2;
    int outputResistance = Math.max(
      (startingResistance - timesAttacked * resistanceDecrease),
      finalResistance
    );
    timesAttacked++;
    return outputResistance;
  }
}
