package edu.ntnu.idatt2001.units;

import edu.ntnu.idatt2001.enums.UnitType;

/**
 * The type Commander unit.
 */
public class CommanderUnit extends CavalryUnit {

  /**
   * Instantiates a new Commander unit.
   *
   * @param name   the name of this Commander unit
   * @param health the starting health of this commander unit.
   * @param armor  the armor of this commander unit.
   * @param attack the attack of this commander unit.
   * @throws IllegalArgumentException the illegal argument exception is thrown when name or health is less than or
   * equal to zero, or if name is blank.
   */
  public CommanderUnit(String name, int health, int armor, int attack)
    throws IllegalArgumentException {
    super(name, health, armor, attack);
  }

  /**
   * Instantiates a new Commander unit.
   *
   * @param name   the name of this Commander unit.
   * @param health the starting health of this Commander unit.
   * @throws IllegalArgumentException the illegal argument exception is thrown when name or health is less than or
   * equal to zero, or if name is blank.
   */
  public CommanderUnit(String name, int health)
    throws IllegalArgumentException {
    super(name, health, 15, 25);
  }

  /**
   * Gets the unit type enum of this unit.
   *
   * @return the unit type enum of this unit (UnitType.COMMANDER)
   */
  @Override
  public UnitType getUnitType() {
    return UnitType.COMMANDER;
  }
}
