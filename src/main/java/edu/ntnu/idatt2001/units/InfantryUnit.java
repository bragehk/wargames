package edu.ntnu.idatt2001.units;

import edu.ntnu.idatt2001.Unit;
import edu.ntnu.idatt2001.enums.Terrain;
import edu.ntnu.idatt2001.enums.UnitType;

/**
 * The type Infantry unit.
 */
public class InfantryUnit extends Unit {

  /**
   * Instantiates a new Infantry unit.
   *
   * @param name   the name of this Infantry unit.
   * @param health the starting health of this Infantry unit.
   * @param armor  the armor of this Infantry unit. This value decides how much damage this unit takes from attacks.
   * @param attack the attack of this Infantry unit.
   * @throws IllegalArgumentException the illegal argument exception is thrown when name or health is less than or
   * equal to zero, or if name is blank.
   */
  public InfantryUnit(String name, int health, int armor, int attack)
    throws IllegalArgumentException {
    super(name, health, armor, attack);
  }

  /**
   * Instantiates a new Infantry unit.
   *
   * @param name   the name of this Infantry unit.
   * @param health the starting health of this Infantry unit.
   * @throws IllegalArgumentException the illegal argument exception is thrown when name or health is less than or
   * equal to zero, or if name is blank.
   */
  public InfantryUnit(String name, int health) throws IllegalArgumentException {
    super(name, health, 10, 15);
  }

  /**
   * Gets the unit type enum of this unit.
   *
   * @return the unit type enum of this unit (UnitType.INFANTRY)
   */
  @Override
  public UnitType getUnitType() {
    return UnitType.INFANTRY;
  }

  /**
   * This is the attack bonus that is used to calculate an attack. If the unit is in the forest terrain, it has a
   * attack bonus of 4. On other terrain it has a bonus of 2.
   * @return 2 or 4, depending on the terrain.
   */
  @Override
  public int getAttackBonus(Terrain terrain) {
    if(terrain == Terrain.FOREST) return 4;
    else return 2;
  }

  /**
   * This is the attack bonus that is used to calculate an attack. If the unit is in the forest terrain, it has an
   * resist bonus of 3. On other terrain it has a bonus of 1.
   * @return 1 or 3, depending on the terrain.
   */
  @Override
  public int getResistBonus(Terrain terrain) {
    if(terrain == Terrain.FOREST) return 3;
    else return 1;
  }
}
