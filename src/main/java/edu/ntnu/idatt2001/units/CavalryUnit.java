package edu.ntnu.idatt2001.units;

import edu.ntnu.idatt2001.Unit;
import edu.ntnu.idatt2001.enums.Terrain;
import edu.ntnu.idatt2001.enums.UnitType;

/**
 * The type Cavalry unit.
 */
public class CavalryUnit extends Unit {

  private boolean hasChargeBonus = true;

  /**
   * Instantiates a new Cavalry unit.
   *
   * @param name   the name of this Cavalry unit
   * @param health the starting health of this Cavalry unit.
   * @param armor  the armor of this Cavalry unit.This value decides how much damage this unit takes from attacks.
   * @param attack the attack of this Cavalry unit.
   * @throws IllegalArgumentException the illegal argument exception is thrown when name or health is less than or
   * equal to zero, or if name is blank.
   */
  public CavalryUnit(String name, int health, int armor, int attack)
    throws IllegalArgumentException {
    super(name, health, armor, attack);
  }

  /**
   * Instantiates a new Cavalry unit.
   *
   * @param name   the name of this Cavalry unit.
   * @param health the starting health of this Cavalry unit.
   * @throws IllegalArgumentException the illegal argument exception
   */
  public CavalryUnit(String name, int health) throws IllegalArgumentException {
    super(name, health, 12, 20);
  }

  /**
   * Gets the unit type enum of this unit.
   *
   * @return the unit type enum of this unit (UnitType.CAVALRY)
   */
  @Override
  public UnitType getUnitType() {
    return UnitType.CAVALRY;
  }

  /**
   * This method calculates the attack bonus that the Cavalry unit gets. When the unit attacks for the first time, it
   * has a charge bonus that gives it +6 attack in the plains terrain, and +4 on other terrains. The base attack bonus
   * is 2. When the unit is in the plains terrain, the base attack bonus is 4.
   * @return The bonus based on what terrain the unit is on.
   */
  @Override
  public int getAttackBonus(Terrain terrain) {
    if(terrain == Terrain.PLAINS){
      if (hasChargeBonus) {
        hasChargeBonus = false;
        return 10;
      } else {
        return 4;
      }
    }else{
      if (hasChargeBonus) {
        hasChargeBonus = false;
        return 6;
      } else {
        return 2;
      }
    }

  }

  /**
   * This is the attack bonus that is used to calculate an attack. Resist bonus is not affected by terrain for the
   * cavalry unit.
   * @return 1
   */
  @Override
  public int getResistBonus(Terrain terrain) {
    if(terrain == Terrain.FOREST) return 0;
    else return 1;
  }
}
