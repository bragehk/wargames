package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.enums.Terrain;
import edu.ntnu.idatt2001.enums.UnitType;

import java.util.Objects;

/**
 * This is a unit class. This class is the super class for every unit in an army.
 */
public abstract class Unit {

  private final String name;
  private final int armor, attack;
  private int health;

  /**
   * Instantiates a new Unit.
   *
   * @param name   the name of your unit
   * @param health the starting health of your unit
   * @param armor  the armor of your unit
   * @param attack the attack of your unit
   * @throws IllegalArgumentException the illegal argument exception throws exception if you enter incorrect values.
   */
  public Unit(String name, int health, int armor, int attack)
    throws IllegalArgumentException {
    if (name.isBlank()) throw new IllegalArgumentException(
      "You have not entered a valid name for the unit."
    );
    if (health <= 0) throw new IllegalArgumentException(
      "Starting health can't be 0 or less than zero."
    );
    if (attack < 0) throw new IllegalArgumentException(
      "Starting attack can't be less than 0."
    );
    if (armor < 0) throw new IllegalArgumentException(
      "Starting armor can't be less than 0."
    );
    this.name = name;
    this.health = health;
    this.armor = armor;
    this.attack = attack;
  }

  /**
   * This method is used when two units attack each other.
   *
   * @param opponent the opponent unit that is getting attacked.
   */
  public void attack(Unit opponent, Terrain terrain) {
    int newHealth =
      opponent.getHealth() -
      (this.attack + this.getAttackBonus(terrain)) +
      (opponent.getArmor() + opponent.getResistBonus(terrain));
    if(newHealth < opponent.getHealth()) {
      opponent.setHealth(newHealth);
    }

  }

  /**
   * Gets attack bonus. This method get initialized by other classes. The bonus is dependent on what terrain the unit
   * is fighting in.
   *
   * @return the attack bonus
   */
  public abstract int getAttackBonus(Terrain terrain);

  /**
   * Gets resist bonus. This method get initialized by other classes. The bonus is dependent on what terrain the unit
   * is fighting in.
   *
   * @return the resist bonus
   */
  public abstract int getResistBonus(Terrain terrain);

  /**
   * Gets Unit Type.
   *
   * @return the unit type
   */
  public abstract UnitType getUnitType();

  /**
   * Sets health.
   *
   * @param health the health
   */
  public void setHealth(int health) {
    this.health = health;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Gets health.
   *
   * @return the health
   */
  public int getHealth() {
    return health;
  }

  /**
   * Gets armor.
   *
   * @return the armor
   */
  public int getArmor() {
    return armor;
  }

  /**
   * Gets attack.
   *
   * @return the attack
   */
  public int getAttack() {
    return attack;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Unit unit = (Unit) o;
    return armor == unit.armor && attack == unit.attack && health == unit.health && Objects.equals(name, unit.name);
  }

  @Override
  public String toString() {
    return (
      "The "+ this.getClass().getSimpleName()+ " named " +
      name +
      " has " +
      health +
      " HP, " +
      attack +
      " attack and " +
      armor +
      " armor"
    );
  }
}
