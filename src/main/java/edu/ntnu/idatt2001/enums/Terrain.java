package edu.ntnu.idatt2001.enums;

/**
 * This is an enum class that represents the terrains Hill, Plains and Forest.
 */
public enum Terrain {
    /**
     * Hill terrain.
     */
    HILL,
    /**
     * Plains terrain.
     */
    PLAINS,
    /**
     * Forest terrain.
     */
    FOREST
}
