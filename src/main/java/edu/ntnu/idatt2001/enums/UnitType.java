package edu.ntnu.idatt2001.enums;

/**
 * The enum class that represents the different unit types. The unit types are Cavalry, Infantry, commander and ranged.
 */
public enum UnitType {
    /**
     * Cavalry unit type.
     */
    CAVALRY,
    /**
     * Commander unit type.
     */
    COMMANDER,
    /**
     * Ranged unit type.
     */
    RANGED,
    /**
     * Infantry unit type.
     */
    INFANTRY;
}
