package edu.ntnu.idatt2001;


import edu.ntnu.idatt2001.units.CavalryUnit;
import edu.ntnu.idatt2001.units.CommanderUnit;
import edu.ntnu.idatt2001.units.InfantryUnit;
import edu.ntnu.idatt2001.units.RangedUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * Creates a new army. An army has units.
 */
public class Army {

  private String name;
  private final ArrayList<Unit> units;

  /**
   * Instantiates a new Army.
   *
   * @param name  The name of the army. This can for example be "Alliance" or "Horde".
   * @param units Arraylist of units that the army consists of.
   * @throws IllegalArgumentException The illegal argument exception is thrown if the army does not have a name.
   */
  public Army(String name, ArrayList<Unit> units) throws IllegalArgumentException {
    if (name.isBlank()) throw new IllegalArgumentException("This army needs a name!");
    this.name = name;
    this.units = units;
  }

  /**
   * Instantiates a new Army.
   *
   * @param name The name of the army. This can for example be "Alliance" or "Horde".
   * @throws IllegalArgumentException The illegal argument exception is thrown if the army does not have a name.
   */
  public Army(String name) throws IllegalArgumentException {
    if (name.isBlank()) throw new IllegalArgumentException("This army needs a name!");
    this.name = name;
    this.units = new ArrayList<>();
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Gets all units.
   *
   * @return the all units
   */
  public ArrayList<Unit> getAllUnits() {
    return units;
  }

  /**
   * Add unit in army.
   *
   * @param unit the unit you want to add to this army.
   * @return true if successful, false if failed.
   */
  public boolean add(Unit unit) {
    return this.units.add(unit);
  }

  /**
   * Add a ArrayList of units in the army.
   *
   * @param units the units you want to add to this army.
   * @return true if successful, false if failed.
   */
  public boolean addAll(ArrayList<Unit> units) {
    if (!units.isEmpty()) {
      return this.units.addAll(units);
    }
    return false;
  }

  /**
   * Sets the name of the army.
   *
   * @param name The new name of the army.
   */
  public void setName(String name) {
    if(name.isBlank()) throw new IllegalArgumentException("This army needs a name!");
    this.name = name;
  }

  /**
   * Remove a unit.
   *
   * @param unit the unit you want to remove from this army.
   * @return true if successful, false if failed.
   */
  public boolean remove(Unit unit) {
    return units.remove(unit);
  }

  /**
   * A boolean that shows if the army has units.
   *
   * @return true if there are units, false if there are no units.
   */
  public boolean hasUnits() {
    return !units.isEmpty();
  }

  /**
   * Gets a random unit from the army.
   *
   * @return the random unit.
   */
  public Unit getRandom() {
    if (!units.isEmpty()) {
      int randInt = new Random().nextInt(units.size());
      return units.get(randInt);
    }
    return null;
  }

  /**
   * Gets cavalry units.
   *
   * @return the cavalry units
   */
  public List<Unit> getCavalryUnits() {
    return units.stream().filter(p -> !(p instanceof CommanderUnit) && p instanceof CavalryUnit).toList();
  }

  /**
   * Gets commander units.
   *
   * @return the commander units
   */
  public List<Unit> getCommanderUnits() {
    return units.stream().filter(p -> p instanceof CommanderUnit).toList();
  }

  /**
   * Gets infantry units.
   *
   * @return the infantry units
   */
  public List<Unit> getInfantryUnits() {
    return units.stream().filter(p -> p instanceof InfantryUnit).toList();
  }

  /**
   * Gets ranged units.
   *
   * @return the ranged units
   */
  public List<Unit> getRangedUnits() {
    return units.stream().filter(p -> p instanceof RangedUnit).toList();
  }

  @Override
  public String toString() {
    int numUnits = this.getAllUnits().size();
    String output;
    if(numUnits>1){
      output = "\n-------------------\n" +
              "\n" + name +
              "\n-------------------\n" +
              "This army has: " + numUnits + " units.\n";
    }else{
      output = "\n-------------------\n" +
              "\n" + name +
              "\n-------------------\n" +
              "This army has: " + numUnits + " unit.\n";
    }
    return output;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Army)) return false;
    Army army = (Army) o;
    return Objects.equals(name, army.name) && units.size() == army.getAllUnits().size() &&
            getRangedUnits().size() == army.getRangedUnits().size() &&
            getCommanderUnits().size() == army.getCommanderUnits().size() &&
            getInfantryUnits().size() == army.getInfantryUnits().size() &&
            getCavalryUnits().size() == army.getCavalryUnits().size();

  }

  @Override
  public int hashCode() {
    return Objects.hash(name, units);
  }
}
