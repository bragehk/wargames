package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.enums.Terrain;

import java.util.ArrayList;
import java.util.Random;

/**
 * This class is made for simulating battles between two armies.
 */
public class Battle {

  private final Army armyOne;
  private final Army armyTwo;
  private final Terrain terrain;
  private final ArrayList<String> battleLog = new ArrayList<>();

  /**
   * Instances a new Battle that has no terrain.
   *
   * @param armyOne the first battle army.
   * @param armyTwo the other battle army.
   * @throws IllegalArgumentException the illegal argument exception is thrown if the armies does not have a name.
   */
  public Battle(Army armyOne, Army armyTwo) throws IllegalArgumentException {
    if (!armyOne.hasUnits()) throw new IllegalArgumentException("Army one has no units!");
    if (!armyTwo.hasUnits()) throw new IllegalArgumentException("Army two has no units!");
    if (armyOne.equals(armyTwo)) throw new IllegalArgumentException("Army one can not be equal to army two");
    this.armyOne = armyOne;
    this.armyTwo = armyTwo;
    this.terrain = null;
  }

  /**
   * Instantiates a new Battle with a terrain parameter. The terrain will determine strengths and weaknesses of
   * certain units.
   *
   * @param armyOne the first battle army.
   * @param armyTwo the other battle army.
   * @param terrain the terrain where the battle is taking place.
   * @throws IllegalArgumentException
   */
  public Battle(Army armyOne, Army armyTwo, Terrain terrain) throws IllegalArgumentException {
    if (!armyOne.hasUnits()) throw new IllegalArgumentException("Army one has no units!");
    if (!armyTwo.hasUnits()) throw new IllegalArgumentException("Army two has no units!");
    if (armyOne.equals(armyTwo)) throw new IllegalArgumentException("Army one can not be equal to army two");
    this.armyOne = armyOne;
    this.armyTwo = armyTwo;
    this.terrain = terrain;
  }

  /**
   * This method is a simulation of a battle between two armies.
   *
   * @return the winning army
   */
  public Army Simulate() throws UnsupportedOperationException {
    if(!armyOne.hasUnits() || !armyTwo.hasUnits()) {
      throw new UnsupportedOperationException("One of the armies has no units!");
    }
    while(armyOne.hasUnits() && armyTwo.hasUnits()){
      Unit unitOne = armyOne.getRandom();
      Unit unitTwo = armyTwo.getRandom();

      int randInt = new Random().nextInt(2);

      if(randInt == 0) {
        int oldHealth = unitTwo.getHealth();
        unitOne.attack(unitTwo, terrain);
        battleLog.add(attackLog(unitOne, unitTwo, oldHealth - unitTwo.getHealth()));
      }else {
        int oldHealth = unitOne.getHealth();
        unitTwo.attack(unitOne, terrain);
        battleLog.add(attackLog(unitTwo, unitOne, oldHealth - unitOne.getHealth()));
      }
      if(unitOne.getHealth()<=0) {
        battleLog.add(killLog(unitTwo, unitOne));
        armyOne.remove(unitOne);
      }
      if(unitTwo.getHealth()<=0) {
        battleLog.add(killLog(unitOne, unitTwo));
        armyTwo.remove(unitTwo);
      }
    }
    if(armyOne.hasUnits()) {
      return armyOne;
    }else {
      return armyTwo;
    }
  }

  /**
   * This method returns the log of an attack.
   *
   * @return the battle log
   */
  private String attackLog(Unit attacker, Unit defender, int damage) {
    return attacker.toString() + " attacked " + defender.toString() + " for " + damage + " damage.";
  }

  /**
   * This method returns the log of a kill.
   *
   * @return the battle log
   */
  private String killLog(Unit attacker, Unit defender) {
    return attacker.toString() + " killed " + defender.toString() + ".";
  }

  /**
   * Gets the battle log.
   *
   * @return the battle log
   */
  public ArrayList<String> getBattleLog() {
    return battleLog;
  }

  @Override
  public String toString() {
    if(armyOne.hasUnits() && armyTwo.hasUnits()){
      return armyOne.toString() + "\n" + armyTwo.toString();
    }else if(armyOne.hasUnits()) {
      return armyOne.toString();
    } else {
      return armyTwo.toString();
    }

  }
}
