package edu.ntnu.idatt2001.GUI;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Wargames application class for setting up the GUI.
 */
public class WargamesApp extends Application {
    /**
     * The entry point of application. Sets default parameters and stage and scene settings.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Sets up the GUI.
     *
     * @param stage the primary stage
     * @throws IOException if the FXML file is not found
     */
    @Override
    public void start(Stage stage) {
        FXMLLoader fxmlLoader = new FXMLLoader(WargamesApp.class.getClassLoader().getResource("Wargames.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(),600,400);
            scene.getStylesheets().add("style.css");
        } catch (IOException e) {
            e.printStackTrace();
        }

        stage.setMinWidth(900);
        stage.setMinHeight(700);
        stage.setMaximized(true);

        stage.setTitle("Wargames");
        stage.setScene(scene);
        stage.show();
    }
}
