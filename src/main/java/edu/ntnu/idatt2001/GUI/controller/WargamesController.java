package edu.ntnu.idatt2001.GUI.controller;

import edu.ntnu.idatt2001.Army;
import edu.ntnu.idatt2001.Battle;
import edu.ntnu.idatt2001.GUI.WargamesApp;
import edu.ntnu.idatt2001.Unit;
import edu.ntnu.idatt2001.enums.Terrain;
import edu.ntnu.idatt2001.factory.UnitFactory;
import edu.ntnu.idatt2001.fileconversion.CSVConverter;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


/**
 * This controller class is responsible for the Wargames main tab.
 */
public class WargamesController implements Initializable {

    @FXML private Label winnerText, score, army1Name, army2Name;
    @FXML private TextField filePath1, filePath2;
    @FXML private ChoiceBox<Terrain> terrainChoice;
    @FXML private ListView<Unit> army1List, army2List;
    @FXML private ListView<String> battleLog;
    @FXML private GridPane background;

    private final FileChooser fc = new FileChooser();
    private final CSVConverter csvConverter = new CSVConverter();
    private final UnitFactory factory = new UnitFactory();
    private final Army[] armies = new Army[2];
    private final Army[] savedArmies = new Army[2];
    private final TextField[] armyFilePaths = new TextField[2];
    private final Label[] armyNames = new Label[2];

    private int team1Wins = 0;
    private int team2Wins = 0;

    /**
     * Resets score between army.
     */
    @FXML
    public void resetScore() {
        team1Wins = 0;
        team2Wins = 0;
        score.setText(team1Wins + " - " + team2Wins);
    }

    /**
     * This method is called when the "Import Army file" button is pressed. It opens a file chooser and the user can
     * choose a CSV file that contains the army.
     *
     * @throws FileNotFoundException the file not found exception is thrown if the file is in the wrong format or is not found
     */
    @FXML
    public void chooseArmy1() throws FileNotFoundException {
        openFileChooser(0);
    }

    /**
     * This method is called when the "Import Army file" button is pressed. It opens a file chooser and the user can
     * choose a CSV file that contains the army.
     *
     * @throws FileNotFoundException the file not found exception is thrown if the file is in the wrong format
     */
    @FXML
    public void chooseArmy2() throws FileNotFoundException {
       openFileChooser(1);
    }

    /**
     * Opens a file chooser and sets the file path in the text field. If the file is not found, an error message is shown.
     * The chosen file must be a .CSV file. The file is then converted to an army and added to the army list.
     *
     * @param armyNumber the army number. 0 for army 1, 1 for army 2.
     */
    private void openFileChooser(int armyNumber) throws FileNotFoundException, IllegalArgumentException {
        if(!(armyNumber == 0) && !(armyNumber == 1)) throw new IllegalArgumentException("Army number must be 0 or 1");
        File file = fc.showOpenDialog(null);
        if (file != null && file.getName().endsWith(".csv")) {
            armies[armyNumber] = csvConverter.readCSV(file.getAbsolutePath());
            savedArmies[armyNumber] = new Army(armies[armyNumber].getName(), factory.deepCopyUnits(armies[armyNumber].getAllUnits()));
            armyFilePaths[armyNumber].setText(file.getAbsolutePath());
            armyNames[armyNumber].setText(armies[armyNumber].getName());
            updateLists();
            return;
        }
        errorBox("This is not a valid CSV file.");
    }


    /**
     * Reset armies to their original state and update lists.
     *
     * @return true if the armies are reset, false if not.
     */
    @FXML
    public boolean resetArmies() {
        if(armies[0] == null || armies[1] == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "One of the armies has not been imported");
            alert.showAndWait();
            return false;
        }else {
            armies[0] = new Army(savedArmies[0].getName(), factory.deepCopyUnits(savedArmies[0].getAllUnits()));
            armies[1] = new Army(savedArmies[1].getName(), factory.deepCopyUnits(savedArmies[1].getAllUnits()));
            updateLists();
            return true;
        }
    }

    /**
     * Reset armies and starts a battle.
     */
    @FXML
    public void resetArmiesAndBattle() {
        if(resetArmies()) startBattle();
    }

    /**
     * Start battle between two armies. The battle is simulated and the results are displayed in the battle log.
     * The winner is displayed in the middle of the screen.
     */
    @FXML
    public void startBattle() {
        if (armies[0] == null || armies[1] == null) {
            errorBox("One of the armies has no units");
            return;
        }
        if (!armies[0].hasUnits() || !armies[1].hasUnits()) {
            errorBox("One of the armies has no units");
            return;
        }
        if(armies[0].hasUnits() && armies[1].hasUnits()) {
            Battle battle = new Battle(armies[0], armies[1], terrainChoice.getValue());
            Army winner = battle.Simulate();
            updateLists();
            battleLog.getItems().addAll(battle.getBattleLog());
            if (armies[0].getName().equals(winner.getName())) {
                winnerText.setText(armies[0].getName() + " wins!");
                team1Wins++;
                score.setText(team1Wins + " - " + team2Wins);
            }else if(armies[1].getName().equals(winner.getName())) {
                winnerText.setText(armies[1].getName() + " wins!");
                team2Wins++;
                score.setText(team1Wins + " - " + team2Wins);
            }else {
                errorBox("Both armies have been destroyed");
            }
        }
    }

    /**
     * This method updates the lists of the armies for it to display the current state of the armies.
     */
    private void updateLists() {
        army1List.getItems().clear();
        army2List.getItems().clear();
        if(armies[0] != null) {
            for (Unit unit : armies[0].getAllUnits()) {
                army1List.getItems().add(unit);
            }
        }
        if (armies[1] != null) {
            for (Unit unit : armies[1].getAllUnits()) {
                army2List.getItems().add(unit);
            }
        }
    }

    /**
     * Error box with a custom error message.
     *
     * @param message custom error message
     */
    private void errorBox(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message);
        alert.showAndWait();
    }

    /**
     * Open the army creator window. Here you can create armies, and you can import armies from a file.
     */
    @FXML
    public void openArmyCreator() {
        FXMLLoader fxmlLoader = new FXMLLoader(WargamesApp.class.getClassLoader().getResource("UnitCreator.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(),600,400);
            scene.getStylesheets().add("style.css");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setMaxHeight(700);
        stage.setMaxWidth(1000);
        stage.setMinWidth(600);
        stage.setMinHeight(500);
        stage.setMaximized(true);
        stage.show();
    }


    /**
     * Sets up the correct startup settings for the application.
     *
     * @param url location of the resource
     * @param resourceBundle resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        armyFilePaths[0] = filePath1;
        armyFilePaths[1] = filePath2;
        armyNames[0] = army1Name;
        armyNames[1] = army2Name;

        fc.setInitialDirectory(new File("./src/main/resources/armyFiles"));
        filePath1.setEditable(false);
        filePath2.setEditable(false);
        terrainChoice.setItems(FXCollections.observableArrayList(Terrain.values()));
        terrainChoice.setOnAction(this::changeBackground);
        terrainChoice.setValue(Terrain.HILL);

    }

    /**
     * This method changes the background of the application based on the terrain selected.
     *
     * @param event event that triggers the method
     */
    private void changeBackground(ActionEvent event) {
        switch (terrainChoice.getValue()) {
            case FOREST -> background.setStyle("-fx-background-image: url(Forest.jpeg)");
            case HILL -> background.setStyle("-fx-background-image: url(Hill.jpeg)");
            case PLAINS -> background.setStyle("-fx-background-image: url(Plains.jpeg)");
        }
    }
}