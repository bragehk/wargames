package edu.ntnu.idatt2001.GUI.controller;

import edu.ntnu.idatt2001.Army;
import edu.ntnu.idatt2001.Unit;
import edu.ntnu.idatt2001.enums.UnitType;
import edu.ntnu.idatt2001.factory.UnitFactory;
import edu.ntnu.idatt2001.fileconversion.CSVConverter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

/**
 * The type Army creator controller.
 */
public class ArmyCreatorController implements Initializable {

    @FXML private TextField cavalryUnits, commanderUnits, infantryUnits, rangedUnits,
            unitAttack, unitDefense, unitHealth, unitName;
    @FXML private BorderPane unitCreatorBackground;
    @FXML private ComboBox<UnitType> unitTypeChoice;
    @FXML private TextField armyName;
    @FXML private ListView<String> armyList;
    @FXML private CheckBox detailedInfo;

    private Army tmpArmy;

    private final FileChooser fc = new FileChooser();
    private final DirectoryChooser dc = new DirectoryChooser();
    private final CSVConverter converter = new CSVConverter();
    private final UnitFactory factory = new UnitFactory();


    /**
     * Add multiple units to army. Input field dictates how many units gets added.
     */
    @FXML
    public void addMultipleUnitsToArmy() {
        int commander;
        int infantry;
        int ranged;
        int cavalry;

        try {
            commander = Integer.parseInt(commanderUnits.getText());
        } catch (NumberFormatException e) {
            errorBox("Commander must be a number.");
            return;
        }
        try {
            infantry = Integer.parseInt(infantryUnits.getText());
        } catch (NumberFormatException e) {
            errorBox("Infantry must be a number.");
            return;
        }
        try {
            ranged = Integer.parseInt(rangedUnits.getText());
        } catch (NumberFormatException e) {
            errorBox("Ranged must be a number.");
            return;
        }
        try {
            cavalry = Integer.parseInt(cavalryUnits.getText());
        } catch (NumberFormatException e) {
            errorBox("Cavalry must be a number.");
            return;
        }
        if (commander < 0 || infantry < 0 || ranged < 0 || cavalry < 0) {
            errorBox("Units must be positive.");
        } else if (tmpArmy == null) {
            errorBox("No army selected.");
        } else{
            addUnitsToArmy(commander, infantry, ranged, cavalry);
        }
    }

    /**
     * Add single unit to army. The unit is specified by the input fields in the GUI.
     */
    @FXML
    public void addSingleUnitToArmy() {
        int health;
        int attack;
        int defense;

        try {
            attack = Integer.parseInt(unitAttack.getText());
        } catch (NumberFormatException e) {
            errorBox("Attack must be a number.");
            return;
        }
        try {
            defense = Integer.parseInt(unitDefense.getText());
        } catch (NumberFormatException e) {
            errorBox("Defense must be a number.");
            return;
        }
        try {
            health = Integer.parseInt(unitHealth.getText());
        } catch (NumberFormatException e) {
            errorBox("Health must be a number.");
            return;
        }
        String name = unitName.getText();
        UnitType unitType = unitTypeChoice.getValue();
        if (unitType == null) {
            errorBox("You must select a unit type!");
        }else if (tmpArmy == null) {
            errorBox("You must select an army!");
        } else {
            addUnitToArmy(unitType, name, health, attack, defense);
        }
    }

    /**
     * Opens file chooser. User can choose a file to load. If the file is a valid CSV file, it is loaded.
     *
     * @throws FileNotFoundException the file not found exception
     */
    @FXML
    public void openFileChooser() throws FileNotFoundException {
        File file = fc.showOpenDialog(null);
        if (file != null && file.getName().endsWith(".csv")) {
            tmpArmy = converter.readCSV(file.getAbsolutePath());
            armyName.setText(tmpArmy.getName());
            updateCorrectList();
            return;
        }
        errorBox("This is not a valid CSV file.");
    }

    /**
     * Error box with a custom error message.
     *
     * @param message custom error message
     */
    private void errorBox(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message);
        alert.showAndWait();
    }

    /**
     *  Adds all units from the input fields to the army. The units are specified by the input fields in the GUI.
     *  The army info list is updated if the units are added successfully.
     *
     * @param cavalryUnits the number of cavalry units you want to add
     * @param commanderUnits the number of commander units you want to add
     * @param infantryUnits the number of infantry units you want to add
     * @param rangedUnits the number of ranged units you want to add
     */
    private void addUnitsToArmy(int cavalryUnits, int commanderUnits, int infantryUnits, int rangedUnits) {
        if(cavalryUnits < 0){
            errorBox("Can't create a negative amount of Cavalry units");
            return;
        }
        if(commanderUnits < 0){
            errorBox("Can't create a negative amount of Commander units");
            return;
        }
        if(infantryUnits < 0){
            errorBox("Can't create a negative amount of Infantry units");
            return;
        }
        if(rangedUnits < 0){
            errorBox("Can't create a negative amount of Ranged units");
            return;
        }
        tmpArmy.addAll(factory.createUnits(cavalryUnits, commanderUnits, infantryUnits, rangedUnits));
        updateCorrectList();
    }

    /**
     * Adds a unit to army. The unit is specified by the input fields in the GUI. The army info list is updated if the
     * unit is added successfully.
     *
     * @param unitType the unit type
     * @param name     the name
     * @param health   the health
     * @param defense  the defense
     * @param attack   the attack
     */
    public void addUnitToArmy(UnitType unitType, String name, int health, int defense, int attack) {
        if(name.isBlank()) {
            errorBox("Please enter a name for the unit");
            return;
        }
        if(health <= 0) {
            errorBox("Please enter a positive health value");
            return;
        }
        if(defense <= 0) {
            errorBox("Please enter a positive defense value");
            return;
        }
        if(attack <= 0) {
            errorBox("Please enter a positive attack value");
            return;
        }
        tmpArmy.add(factory.createSingleUnit(unitType, name, health, defense, attack));
        updateCorrectList();
    }

    /**
     * Updates the army info list with the correct list depending on the detailed info checkbox.
     */
    private void updateCorrectList() {
        if(detailedInfo.isSelected()) {
            detailedUpdateList();
        }else {
            updateList();
        }
    }

    /**
     * Updates the army info list.
     */
    private void updateList() {
        armyList.getItems().clear();
        armyList.getItems().add(tmpArmy.getCommanderUnits().size() + " Commander Units");
        armyList.getItems().add(tmpArmy.getInfantryUnits().size() + " Infantry Units");
        armyList.getItems().add(tmpArmy.getRangedUnits().size() + " Ranged Units");
        armyList.getItems().add(tmpArmy.getCavalryUnits().size() + " Cavalry Units");
    }

    /**
     * Updates the army info list with detailed information.
     */
    private void detailedUpdateList() {
        armyList.getItems().clear();
        for (Unit unit : tmpArmy.getAllUnits()) {
            armyList.getItems().add(unit.toString());
        }
    }

    /**
     * The detailed info switch method is called when the detailed info checkbox is clicked. When the checkbox is
     * selected, the army info list is updated with detailed information. When the checkbox is deselected, the army info
     * list is updated with basic information.
     */
    @FXML
    public void detailedInfoSwitch() {
        if(tmpArmy==null) {
            detailedInfo.setSelected(!detailedInfo.isSelected());
            errorBox("Please create an army first");
            return;
        }
        updateCorrectList();
    }

    /**
     * Write army to a new file.
     *
     * @throws IOException if the file is not found or the file is not writable
     */
    @FXML
    public void writeArmy() throws IOException {
        dc.showDialog(null);
        if (!armyName.getText().isBlank()){
            tmpArmy.setName(armyName.getText());
            converter.writeNewCSV(tmpArmy,dc.getInitialDirectory().getAbsolutePath());
        } else {
            errorBox("Please enter a name for the army");
        }
    }

    /**
     * Sets up the correct startup settings for the application.
     *
     * @param url location of the resource
     * @param resourceBundle resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        fc.setInitialDirectory(new File("./src/main/resources/armyFiles"));
        dc.setInitialDirectory(new File("./src/main/resources/armyFiles"));
        unitTypeChoice.getItems().addAll(UnitType.values());
        unitCreatorBackground.setStyle("-fx-background-image: url(UnitBackground.jpeg)");
    }
}
