package edu.ntnu.idatt2001.GUI;

/**
 * Main class for starting the GUI. Only necessary for creating a Jar file.
 */
public class Main {

    /**
     * First method that is launched when starting the program.
     *
     * @param args - String[] - commandline arguments
     */
    public static void main(String[] args) {
        WargamesApp.main(args);
    }
}
