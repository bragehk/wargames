package edu.ntnu.idatt2001.fileconversion;

import edu.ntnu.idatt2001.Army;
import edu.ntnu.idatt2001.Unit;
import edu.ntnu.idatt2001.units.CavalryUnit;
import edu.ntnu.idatt2001.units.CommanderUnit;
import edu.ntnu.idatt2001.units.InfantryUnit;
import edu.ntnu.idatt2001.units.RangedUnit;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class is responsible for reading and writing armies to CSV-files. The methods in this class can read, write and
 * create armies from CSV-files.
 */
public class CSVConverter {


    /**
     * This method creates a new CSV file with an army. The CSV file has information about the army. The file needs to be
     * in the correct format. The file needs to have the name of the army, the name of the unit, the health of the unit.
     *
     * @param army          the army that is to be converted to a CSV file.
     * @param filePath      the file path is where you want to store the CSV file.
     * @throws IOException  the io exception is thrown if there is an input or output error related to writing the file.
     * @throws FileNotFoundException    this exception is thrown if the file can't be found.
     * @throws FileHandlerException     this exception is thrown if the file is in the wrong format.
     */
    public void writeNewCSV(Army army, String filePath) throws IOException {
        if(army == null) throw new NullPointerException("Army can't be Null.");
        try {
            File newCSVFile = new File(filePath+"/"+army.getName()+".csv");
            if(newCSVFile.createNewFile()) {
                FileWriter fw = new FileWriter(newCSVFile.getAbsolutePath());
                fw.write(army.getName()+"\n");
                for (Unit unit : army.getAllUnits()) {
                    fw.write(unit.getClass().getSimpleName()+","+unit.getName()+","+unit.getHealth()+"\n");
                }
                fw.close();
            }else{
                throw new FileHandlerException("File already exists.");
            }
        } catch (FileNotFoundException e){
            throw new FileNotFoundException("Could not find file.");
        }
    }

    /**
     * Read csv army from a file. The file needs to have information about the army in the correct format.
     * The correct format is: unitType, unitName, unitHealth.
     *
     * @param filePath the file path with the CSV file.
     * @return the army that is in the CSV file.
     * @throws FileNotFoundException the file not found exception is thrown if the CSV file is not in the file path.
     * @throws FileHandlerException this exception is thrown if the file is in the wrong format.
     */
    public Army readCSV(String filePath) throws FileNotFoundException {
        if(!filePath.endsWith(".csv")) throw new FileHandlerException("This is not a CSV file.");

        ArrayList<Unit> units = new ArrayList<>();
        try(Scanner sc = new Scanner(new File(filePath))) {
            String name = sc.next();
            sc.nextLine();
            int lineNumber = 1;

            while (sc.hasNext()){
                try{
                    String[] line = sc.nextLine().split(",");
                    switch (line[0]) {
                        case "RangedUnit" -> units.add(new RangedUnit(line[1], Integer.parseInt(line[2])));
                        case "InfantryUnit" -> units.add(new InfantryUnit(line[1], Integer.parseInt(line[2])));
                        case "CommanderUnit" -> units.add(new CommanderUnit(line[1], Integer.parseInt(line[2])));
                        case "CavalryUnit" -> units.add(new CavalryUnit(line[1], Integer.parseInt(line[2])));
                        default -> {
                            throw new FileHandlerException("Wrong format in line: " + lineNumber);
                        }
                    }
                    lineNumber++;
                }catch (FileHandlerException e) {
                    throw new FileHandlerException("The file could not be read at line "+lineNumber+".");
                }
            }
            sc.close();
            return new Army(name, units);
        } catch (FileHandlerException e) {
            throw new FileHandlerException(e.getMessage());
        }
    }
}