package edu.ntnu.idatt2001.fileconversion;

/**
 * This class throw a new Exception if the file type is wrong, or it's the wrong format.
 */
public class FileHandlerException extends IllegalArgumentException{

    public FileHandlerException(String message) {
        super(message);
    }
}
