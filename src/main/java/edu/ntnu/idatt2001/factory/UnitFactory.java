package edu.ntnu.idatt2001.factory;

import edu.ntnu.idatt2001.Unit;
import edu.ntnu.idatt2001.enums.UnitType;
import edu.ntnu.idatt2001.units.CavalryUnit;
import edu.ntnu.idatt2001.units.CommanderUnit;
import edu.ntnu.idatt2001.units.InfantryUnit;
import edu.ntnu.idatt2001.units.RangedUnit;

import java.util.ArrayList;

/**
 * The UnitFactory class makes it easy to create either a single or multiple units.
 */
public class UnitFactory {

    /**
     * Create a single unit. This unit can either be CavalryUnit, CommanderUnit, InfantryUnit or RangedUnit.
     *
     * @param unitType the unit type can be CavalryUnit, CommanderUnit, InfantryUnit or RangedUnit.
     * @param name     this is the name of the unit.
     * @param health   the health of the unit.
     * @return the unit
     * @throws IllegalArgumentException the illegal argument exception throws if there is a invalid input.
     */
    public Unit createSingleUnit(UnitType unitType, String name, int health) throws IllegalArgumentException{
        switch(unitType) {
            case COMMANDER: return new CommanderUnit(name, health);
            case INFANTRY: return new InfantryUnit(name, health);
            case RANGED: return new RangedUnit(name, health);
            case CAVALRY: return new CavalryUnit(name, health);
        }
        throw new IllegalArgumentException("Invalid unit type.");
    }

    /**
     * Create a single unit. This unit can either be CavalryUnit, CommanderUnit, InfantryUnit or RangedUnit. This unit
     * has custom attack and armor.
     *
     * @param unitType the unit type can be CavalryUnit, CommanderUnit, InfantryUnit or RangedUnit.
     * @param name     this is the name of the unit.
     * @param health   the health of the unit.
     * @return the unit
     * @throws IllegalArgumentException the illegal argument exception throws if there is a invalid input.
     */
    public Unit createSingleUnit(UnitType unitType, String name, int health, int attack, int armor)
            throws IllegalArgumentException{
        switch (unitType) {
            case COMMANDER:
                return new CommanderUnit(name, health, attack, armor);
            case INFANTRY:
                return new InfantryUnit(name, health, attack, armor);
            case RANGED:
                return new RangedUnit(name, health, attack, armor);
            case CAVALRY:
                return new CavalryUnit(name, health, attack, armor);
        }
        throw new IllegalArgumentException("Invalid unit type.");
    }


    /**
     * Create an array list of units. You can choose how many of each unit you want in the array list.
     *
     * @param cavalryUnits   the number of cavalry units you want
     * @param commanderUnits the number of commander units you want
     * @param infantryUnits  the number of infantry units you want
     * @param rangedUnits    the number of ranged units you want
     * @return the array list with all the units.
     * @throws IllegalArgumentException the illegal argument exception is thrown if there is a negative number in the
     * input field.
     */
    public ArrayList<Unit> createUnits(int cavalryUnits, int commanderUnits, int infantryUnits, int rangedUnits)
            throws IllegalArgumentException{
        if(cavalryUnits<0 || commanderUnits<0 || infantryUnits<0 || rangedUnits<0) {
            throw new IllegalArgumentException("Illegal amount of units");
        }
        ArrayList<Unit> outputUnits = new ArrayList<>();
        for(int i = 0; i<cavalryUnits; i++) outputUnits.add(createSingleUnit(UnitType.CAVALRY, "Cavalry"+i, 100));
        for(int i = 0; i<commanderUnits; i++) outputUnits.add(createSingleUnit(UnitType.COMMANDER, "Commander"+i, 100));
        for(int i = 0; i<infantryUnits; i++) outputUnits.add(createSingleUnit(UnitType.INFANTRY, "Infantry"+i, 100));
        for(int i = 0; i<rangedUnits; i++) outputUnits.add(createSingleUnit(UnitType.RANGED, "Ranged"+i, 100));
        return outputUnits;
    }

    /**
     * Deep copies a list of units. This method is used to make sure that the original list is not changed.
     *
     * @param units The ArrayList has to be a list of units.
     * @return the array list with all the units.
     */
    public ArrayList<Unit> deepCopyUnits(ArrayList<Unit> units) {
        ArrayList<Unit> outputUnits = new ArrayList<>();
        for (Unit unit : units) {
            outputUnits.add(createSingleUnit(unit.getUnitType(), unit.getName(), unit.getHealth()));
        }
        return outputUnits;
    }

}
