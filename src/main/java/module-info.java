module edu.ntnu.idatt2001 {
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;


    opens edu.ntnu.idatt2001.GUI.controller to javafx.fxml;
    exports edu.ntnu.idatt2001.GUI.controller;
    exports edu.ntnu.idatt2001.GUI;
    opens edu.ntnu.idatt2001.GUI to javafx.fxml;
}