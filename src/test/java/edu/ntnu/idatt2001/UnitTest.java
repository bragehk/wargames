package edu.ntnu.idatt2001;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.enums.Terrain;
import edu.ntnu.idatt2001.units.CavalryUnit;
import edu.ntnu.idatt2001.units.CommanderUnit;
import edu.ntnu.idatt2001.units.InfantryUnit;
import edu.ntnu.idatt2001.units.RangedUnit;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class UnitTest {

  @Nested
  public class ResistanceBonusWithoutTerrainTest {

    @Test
    public void Test_Resistance_In_Cavalry_Unit() {
      CavalryUnit cavalryUnit = new CavalryUnit("Eilert", 100);
      assertEquals(1, cavalryUnit.getResistBonus(null));
    }

    @Test
    public void Test_Resistance_In_Commander_Unit() {
      CommanderUnit commanderUnit = new CommanderUnit("Eilert", 100);
      assertEquals(1, commanderUnit.getResistBonus(null));
    }

    @Test
    public void Test_Resistance_In_Infantry_Unit() {
      InfantryUnit infantryUnit = new InfantryUnit("Eilert", 100);
      assertEquals(1, infantryUnit.getResistBonus(null));
    }

    @Test
    public void Test_Resistance_In_Ranged_Unit() {
      RangedUnit rangedUnit = new RangedUnit("Eilert", 1000);
      assertEquals(6, rangedUnit.getResistBonus(null));
      assertEquals(4, rangedUnit.getResistBonus(null));
      assertEquals(2, rangedUnit.getResistBonus(null));
      assertEquals(2, rangedUnit.getResistBonus(null));
    }
  }

  @Nested
  public class ResistanceBonusWithTerrainTest{
    @Test
    public void Test_Resistance_In_Cavalry_Unit_In_Forest() {
      CavalryUnit cavalryUnit = new CavalryUnit("Eilert", 100);
      assertEquals(0, cavalryUnit.getResistBonus(Terrain.FOREST));
    }

    @Test
    public void Test_Resistance_In_Commander_Unit_In_Forest() {
      CommanderUnit commanderUnit = new CommanderUnit("Eilert", 100);
      assertEquals(0, commanderUnit.getResistBonus(Terrain.FOREST));
    }

    @Test
    public void Test_Resistance_In_Infantry_Unit_In_Forest() {
      InfantryUnit infantryUnit = new InfantryUnit("Eilert", 100);
      assertEquals(3, infantryUnit.getResistBonus(Terrain.FOREST));
    }
  }

  @Nested
  public class AttackBonusWithoutTerrainTest {

    @Test
    public void Test_Attack_Bonus_In_Cavalry_Unit() {
      CavalryUnit cavalryUnit = new CavalryUnit("Eilert", 100);
      assertEquals(6, cavalryUnit.getAttackBonus(null));
      assertEquals(2, cavalryUnit.getAttackBonus(null));
      assertEquals(2, cavalryUnit.getAttackBonus(null));
    }

    @Test
    public void Test_Attack_Bonus_In_Commander_Unit() {
      CommanderUnit commanderUnit = new CommanderUnit("Eilert", 100);
      assertEquals(6, commanderUnit.getAttackBonus(null));
      assertEquals(2, commanderUnit.getAttackBonus(null));
      assertEquals(2, commanderUnit.getAttackBonus(null));
    }

    @Test
    public void Test_Attack_Bonus_In_InfantryUnit() {
      InfantryUnit infantryUnit = new InfantryUnit("Eilert", 100);
      assertEquals(2, infantryUnit.getAttackBonus(null));
    }

    @Test
    public void Test_Attack_Bonus_In_RangedUnit() {
      RangedUnit rangedUnit = new RangedUnit("Archer", 50);
      assertEquals(3, rangedUnit.getAttackBonus(null));
    }
  }

  @Nested
  public class AttackBonusWithTerrainTest {

    @Test
    public void Test_Attack_Bonus_In_Cavalry_Unit() {
      CavalryUnit cavalryUnit = new CavalryUnit("Eilert", 100);
      assertEquals(10, cavalryUnit.getAttackBonus(Terrain.PLAINS));
      assertEquals(4, cavalryUnit.getAttackBonus(Terrain.PLAINS));
      assertEquals(4, cavalryUnit.getAttackBonus(Terrain.PLAINS));
    }

    @Test
    public void Test_Attack_Bonus_In_Commander_Unit() {
      CommanderUnit commanderUnit = new CommanderUnit("Eilert", 100);
      assertEquals(10, commanderUnit.getAttackBonus(Terrain.PLAINS));
      assertEquals(4, commanderUnit.getAttackBonus(Terrain.PLAINS));
      assertEquals(4, commanderUnit.getAttackBonus(Terrain.PLAINS));
    }

    @Test
    public void Test_Attack_Bonus_In_InfantryUnit() {
      InfantryUnit infantryUnit = new InfantryUnit("Eilert", 100);
      assertEquals(4, infantryUnit.getAttackBonus(Terrain.FOREST));
    }

    @Test
    public void Test_Attack_Bonus_In_RangedUnit_HILL() {
      RangedUnit rangedUnit = new RangedUnit("Archer", 50);
      assertEquals(6, rangedUnit.getAttackBonus(Terrain.HILL));
    }

    @Test
    public void Test_Attack_Bonus_In_RangedUnit_FOREST() {
      RangedUnit rangedUnit = new RangedUnit("Archer", 50);
      assertEquals(1, rangedUnit.getAttackBonus(Terrain.FOREST));
    }
  }

  @Nested
  public class UnitCreationTest {

    @Test
    public void Create_Unit_With_Invalid_Name() {
      assertThrows(Exception.class, () -> new CavalryUnit(" ", 100));
    }

    @Test
    public void Create_Unit_With_Invalid_Health_And_Full_Constructor() {
      assertThrows(Exception.class, () -> new CavalryUnit("Test", 0, 20, 20));
    }

    @Test
    public void Test_Blank_Name_Exception() {
      try {
        new CavalryUnit(" ", 100);
      } catch (IllegalArgumentException e) {
        assertEquals("You have not entered a valid name for the unit.", e.getMessage());
      }
    }

    @Test
    public void Test_Invalid_Health_Exception() {
      try {
        new CavalryUnit("Brage", -312);
      } catch (IllegalArgumentException e) {
        assertEquals("Starting health can't be 0 or less than zero.", e.getMessage());
      }
    }

    @Test
    public void Test_Invalid_Armor_Exception() {
      try {
        new CavalryUnit("Brage", 100, -20, 20);
      } catch (IllegalArgumentException e) {
        assertEquals("Starting armor can't be less than 0.", e.getMessage());
      }
    }

    @Test
    public void Test_Invalid_Attack_Exception() {
      try {
        new CavalryUnit("Brage", 100, 20, -20);
      } catch (IllegalArgumentException e) {
        assertEquals("Starting attack can't be less than 0.", e.getMessage());
      }
    }
  }

  @Nested
  public class TestAttackMethod{

    @Test
    public void attacked_Unit_Loses_Health_Test() {
      CavalryUnit cavalryUnit1 = new CavalryUnit("Eilert", 100);
      CavalryUnit cavalryUnit2 = new CavalryUnit("Carlum", 100);
      cavalryUnit2.attack(cavalryUnit1, null);
      assertNotEquals(100,cavalryUnit1.getHealth());
      assertEquals(100,cavalryUnit2.getHealth());
    }

    @Test
    public void attacking_In_Terrain_Changes_Resistance_And_Attack() {
      /*
      The cavalry unit has 0 resistance bonus in the forest terrain. The Infantry unit has more attack in the forest
      terrain. That is why the difference in the forest is bigger than the difference in other terrain between these
      two units.
       */
      CavalryUnit cavalryUnit = new CavalryUnit("Eilert", 100);
      InfantryUnit infantryUnit = new InfantryUnit("Carlum", 100);
      infantryUnit.attack(cavalryUnit, Terrain.FOREST);
      int hp = cavalryUnit.getHealth();
      int DifferenceInForest = 100 - cavalryUnit.getHealth();

      infantryUnit.attack(cavalryUnit, null);
      int DifferenceNotInForest = hp - cavalryUnit.getHealth();

      if(DifferenceNotInForest > DifferenceInForest) fail();
    }

    @Test
    public void attacked_Unit_Loses_Exactly_10_Health_Test() {
      CavalryUnit attacker = new CavalryUnit("Eilert", 100, 0, 10) {
        @Override
        public int getAttackBonus(Terrain terrain) {
          return 0;
        }

        @Override
        public int getResistBonus(Terrain terrain) {
          return 0;
        }
      };
      CavalryUnit defender = new CavalryUnit("Carlum", 100, 0, 0){
        @Override
        public int getAttackBonus(Terrain terrain) {
          return 0;
        }

        @Override
        public int getResistBonus(Terrain terrain) {
          return 0;
        }
      };
      attacker.attack(defender, null);
      assertEquals(90,defender.getHealth());
    }
  }
}
