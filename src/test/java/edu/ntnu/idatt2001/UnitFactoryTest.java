package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.enums.UnitType;
import edu.ntnu.idatt2001.factory.UnitFactory;
import edu.ntnu.idatt2001.units.CavalryUnit;
import edu.ntnu.idatt2001.units.CommanderUnit;
import edu.ntnu.idatt2001.units.InfantryUnit;
import edu.ntnu.idatt2001.units.RangedUnit;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class UnitFactoryTest {

    private final UnitFactory factory = new UnitFactory();

    @Nested
    public class SingleUnitTest{

        @Test
        public void create_cavalry_unit() {
            Unit testUnit = factory.createSingleUnit(UnitType.CAVALRY, "Per", 100);
            assertEquals(testUnit, new CavalryUnit("Per", 100));
        }

        @Test
        public void create_infantry_unit() {
            Unit testUnit = factory.createSingleUnit(UnitType.INFANTRY, "Pål", 100);
            assertEquals(testUnit, new InfantryUnit("Pål", 100));
        }

        @Test
        public void create_ranged_unit() {
            Unit testUnit = factory.createSingleUnit(UnitType.RANGED, "Espen", 100);
            assertEquals(testUnit, new RangedUnit("Espen", 100));
        }

        @Test
        public void create_commander_unit() {
            Unit testUnit = factory.createSingleUnit(UnitType.COMMANDER, "Askeladden", 100);
            assertEquals(testUnit, new CommanderUnit("Askeladden", 100));
        }
    }

    @Nested
    public class InvalidHealthTest{

        @Test
        public void create_cavalry_unit_with_zero_health_throws_exception() {
            assertThrows(IllegalArgumentException.class, () ->
                    factory.createSingleUnit(UnitType.CAVALRY, "Espen", 0));
        }

        @Test
        public void negative_health_throws_exception() {
            assertThrows(IllegalArgumentException.class,
                    () -> factory.createSingleUnit(UnitType.CAVALRY, "Espen", -1));
        }
    }

    @Nested
    public class CreateMultipleUnitsTest {

        @Test
        public void create_a_CavalryUnit() {
            ArrayList<Unit> units = factory.createUnits(1,0,0,0);
            String unitName = units.get(0).getName();
            assertEquals(units.get(0), new CavalryUnit(unitName,100));
            assertTrue(units.stream().anyMatch(unit -> unit instanceof CavalryUnit));
        }

        @Test
        public void factory_creates_correct_number_of_units() {
            ArrayList<Unit> factoryUnits = factory.createUnits(10,10,5,50);
            assertEquals(75, factoryUnits.size());
        }

        @Test
        public void negative_number_of_units_throws_exception() {
            UnitFactory factory = new UnitFactory();
            assertThrows(IllegalArgumentException.class,
                    () -> factory.createUnits(10,10,-5,50));
        }
    }
}