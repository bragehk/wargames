package edu.ntnu.idatt2001;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.factory.UnitFactory;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class BattleTest {

    private Battle battle;
    private Army alliance;
    private Army horde;

    @BeforeEach
    public void start_A_Battle(){
        UnitFactory factory = new UnitFactory();
        ArrayList<Unit> testUnits1 = factory.createUnits(10,1,20,15);

        ArrayList<Unit> testUnits2 = factory.createUnits(10,1,20,15);

        alliance = new Army("Alliance", testUnits1);
        horde = new Army("Horde", testUnits2);
        battle = new Battle(alliance, horde);
    }


    @Test
    public void is_There_A_Winner() {
        Army winner = battle.Simulate();
        assertTrue(winner.hasUnits());
        System.out.println(winner);
    }

    @Test
    public void the_Looser_Has_No_Units_But_Winner_Has_Units() {
        if(battle.Simulate().equals(alliance)){
            assertTrue(alliance.hasUnits());
            assertFalse(horde.hasUnits());
        }else {
            assertTrue(horde.hasUnits());
            assertFalse(alliance.hasUnits());
        }
    }

    @Test
    public void exception_If_An_Army_Has_No_Units() {
        battle.Simulate(); //removing every unit in one of the armies by doing a battle.
        assertThrows(Exception.class, () -> battle.Simulate());
    }

    @Test
    public void exception_If_An_Army_Attacks_Itself() {
        assertThrows(Exception.class, () -> new Battle(alliance, alliance));
    }
}
