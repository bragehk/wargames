package edu.ntnu.idatt2001;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import edu.ntnu.idatt2001.enums.UnitType;
import edu.ntnu.idatt2001.factory.UnitFactory;
import edu.ntnu.idatt2001.units.CommanderUnit;
import org.junit.jupiter.api.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ArmyTest {

  @Nested
  public class ArmyCreationTest {

    @Test
    public void create_Army_With_Valid_Name() {
      Army army = new Army("MyArmy");
      assertEquals("MyArmy", army.getName());
    }

    @Test
    public void create_Army_With_Blank_Name() {
      assertThrows(Exception.class, () -> new Army(""));
    }

    @Test
    public void create_Army_With_Troops() {
      UnitFactory factory = new UnitFactory();
      ArrayList<Unit> testUnits = factory.createUnits(2,7,34,100);
      Army army = new Army("TestArmy", testUnits);
      assertEquals(testUnits, army.getAllUnits());
      assertTrue(army.hasUnits());
    }
  }

  @Nested
  public class EqualsAndHashCodeTest {

    private Army army1;
    private Army army2;
    private final UnitFactory factory = new UnitFactory();

    @BeforeEach
    public void create_Instance_Of_Two_Equal_Armies() {
      ArrayList<Unit> testUnits1 = factory.createUnits(2,3,4,6);
      army1 = new Army("TestArmy", testUnits1);

      ArrayList<Unit> testUnits2 =factory.createUnits(2,3,4,6);
      army2 = new Army("TestArmy", testUnits2);
    }

    @Test
    public void two_Armies_Are_Equal_Using_Equals_Method() {
      assertEquals(army1, army2);
    }

    @Test
    public void hashCode_Of_Two_Armies_Are_Not_Equal() {
      assertNotEquals(army1.hashCode(), army2.hashCode());
    }

    @Test
    public void hashCode_Of_Two_Different_Armies_Are_Equal_If_Name_And_Units_Are_Equal() {
      /*
      The units in the previous test are the same units, but they are not stored in the same place in the memory.
      Therefore, the hashCode of the two armies are not equal. In this test we create two armies with units that are
      stored in the same place in the memory.
       */
      String name = "CommonArmyName";
      CommanderUnit commonUnit = (CommanderUnit) factory.createSingleUnit(UnitType.COMMANDER,name,100);
      ArrayList<Unit> units1 = new ArrayList<>();
      units1.add(commonUnit);
      ArrayList<Unit> units2 = new ArrayList<>();
      units2.add(commonUnit);
      Army testArmy1 = new Army(name, units1);
      Army testArmy2 = new Army(name, units2);
      assertEquals(testArmy1.hashCode(),testArmy2.hashCode());
    }
  }

  @Nested
  public class ChangeUnitTests {
    private final UnitFactory factory = new UnitFactory();

    @Test
    public void add_Ranged_Unit_In_Army_Test() {
      Army army = new Army("CoolArmy");
      assertTrue(army.add(factory.createSingleUnit(UnitType.RANGED,"Per",100)));
    }

    @Test
    public void add_Multiple_Units_In_Army_Test() {
      ArrayList<Unit> testUnits = factory.createUnits(10,10,10,1);
      Army army = new Army("MyArmy");
      assertTrue(army.addAll(testUnits));
    }

    @Test
    public void remove_All_Units_In_An_Army_Test() {
      ArrayList<Unit> testUnits = factory.createUnits(1,2,10,1);
      int size = testUnits.size();
      Army army = new Army("TestArmy", testUnits);
      for(int i = 0; i < size; i++) {
        assertTrue(army.remove(army.getAllUnits().get(0)));
      }
      assertFalse(army.hasUnits());
    }
  }


  private Army army1;

  @BeforeEach
  public void create_Instance_Of_Army() {
    UnitFactory factory = new UnitFactory();
    ArrayList<Unit> testUnits1 = factory.createUnits(2,3,4,5);
    army1 = new Army("TestArmy", testUnits1);
  }

  @Nested
  public class GetUnitTest {

    @Test
    public void get_Cavalry_Units() {
      assertEquals(2, army1.getCavalryUnits().size());
    }

    @Test
    public void get_Commander_Units() {
      assertEquals(3, army1.getCommanderUnits().size());
    }

    @Test
    public void get_Infantry_Units() {
      assertEquals(4, army1.getInfantryUnits().size());
    }

    @Test
    public void get_Ranged_Units() {
      assertEquals(5, army1.getRangedUnits().size());
    }

    @Test
    public void get_All_Units() {
      assertEquals(14, army1.getAllUnits().size());
    }
  }
}
