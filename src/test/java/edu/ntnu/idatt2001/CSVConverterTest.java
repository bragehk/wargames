package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.factory.UnitFactory;
import edu.ntnu.idatt2001.fileconversion.CSVConverter;
import edu.ntnu.idatt2001.fileconversion.FileHandlerException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

public class CSVConverterTest {

    @TempDir
    public Path tempDir;

    private final UnitFactory factory = new UnitFactory();
    private Army army1;
    private final CSVConverter converter = new CSVConverter();

    @BeforeEach
    public void create_Instance_Of_Army() {
        army1 = new Army("TestArmy", factory.createUnits(3,1,4,2));
    }

    @Nested
    public class WriteNewCSVTest {


        @Test
        public void file_Written_To_Directory_Exists() throws IOException {
            converter.writeNewCSV(army1, tempDir.toString());
            assertTrue(Files.exists(tempDir.resolve("TestArmy.csv")));
        }

        @Test
        public void file_Written_To_Directory_Is_Not_Empty() throws IOException {
            converter.writeNewCSV(army1, tempDir.toString());
            assertNotEquals(0, Files.size(tempDir.resolve("TestArmy.csv")));
        }

        @Test
        public void file_Written_To_Directory_Contains_Correct_Name() throws IOException {
            converter.writeNewCSV(army1, tempDir.toString());
            assertTrue(Files.readString(tempDir.resolve("TestArmy.csv")).startsWith("TestArmy"));
        }

        @Test
        public void file_Written_To_Directory_Contains_A_Unit_In_Correct_Format() throws IOException {
            converter.writeNewCSV(army1, tempDir.toString());
            System.out.println(tempDir.resolve("TestArmy.csv"));
            assertTrue(Files.readString(tempDir.resolve("TestArmy.csv")).contains("InfantryUnit,Infantry0,100"));
        }

        @Test
        public void should_Throw_FileHandlerException_When_Directory_Not_Found() {
            assertThrows(IOException.class, () -> converter.writeNewCSV(army1, "not_found"));
        }

        @Test
        public void should_Throw_FileHandlerException_When_Army_Is_Null() {
            assertThrows(NullPointerException.class, () -> converter.writeNewCSV(null, tempDir.toString()));
        }

        @Test
        public void exception_Is_Thrown_If_File_Is_Not_csv() {
            Path filePath = tempDir.resolve("army1.html");
            assertThrows(IOException.class, () -> converter.writeNewCSV(army1, filePath.toString()));
        }

        @Test
        public void exception_Is_Thrown_If_Army_Is_Null() {
            Path filePath = tempDir.resolve("TestArmy.csv");
            assertThrows(NullPointerException.class, () -> converter.writeNewCSV(null, filePath.toString()));
        }
    }

    @Nested
    public class ReadCSVTest {

        @Test
        public void read_csv_file() throws IOException {
            converter.writeNewCSV(army1, tempDir.toString());
            assertTrue(Files.exists(tempDir.resolve("TestArmy.csv")));
            Army armyFromReadMethod = converter.readCSV(tempDir.resolve("TestArmy.csv").toString());
            assertEquals(army1, armyFromReadMethod);
        }

        @Test
        public void exception_Is_Thrown_If_File_Is_Not_csv() {
            Path filePath = tempDir.resolve("TestArmy.exe");
            assertThrows(FileHandlerException.class, () -> converter.readCSV(filePath.toString()));
        }

        @Test
        public void exception_Is_Thrown_If_File_Does_Not_Exist() {
            Path filePath = tempDir.resolve("TestArmy.csv");
            assertThrows(FileNotFoundException.class, () -> converter.readCSV(filePath.toString()));
        }

        @Test
        public void exception_Is_Thrown_If_File_Is_In_Wrong_Format() throws IOException {
            converter.writeNewCSV(army1, tempDir.toString());
            String filePath = tempDir.resolve("TestArmy.csv").toString();

            FileWriter fw = new FileWriter(filePath);
            fw.write("This"+"\n"+", 13,37, should, 99, destroy, the, file, 1, 1");
            assertThrows(Exception.class, () -> converter.readCSV(filePath));
            fw.close();
        }
    }


}
