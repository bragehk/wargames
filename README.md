# Wargames project report

## introduction

This is a report about the process of making the "Wargames" application. The program is about creating or importing armies and making them fight. Each army has units. In the course IDATT2001 Programming 2, students were given specifications that the application had to follow. Beyond that, the students were free to implement more features.

## Features and specifications

The students had to make a simulation application in JavaFX. The application simulates the battle between two armies. One army is victorious after killing every unit in the other army.

##### GUI

The most important features of the GUI includes:  

 - Importing armies from a CSV file.
 - Buttons to make armies fight and reset the score.
 - Armies can be reset so they can fight again without having to load the armies from a file again.
 - Easy to read the information about the army.
 - A army creator for creating an army and then converting the army to a CSV file


## Design

This section will mention a few important design patterns and principles, and how they are used in the “Wargames” application. Visual design and backend design patterns will be mentioned in this section.

##### MVC

The project used MVC (Model, View, Controller). MVC is a software design pattern. In the project the model part takes care of the business logic. The classes representing armies, units and battles are examples of this. There are two controllers in the project. One for the main page and one for the army creation tool. The controllers take care of the input from the GUI. The view part is the FXML files. The FXML files determines how the GUI looks.

### Visual design principles

##### Keeping it simple

Here is an image of the main page when the application is launched.

![Start-up page](source/images/MainPage.png)

The next image is shown when the *Army File Creation Tool* button is pressed.

![Army Creation Tool](source/images/ArmyCreator.png)

The design of this application is quite simple. There are not too many buttons, and not enough features to make it confusing. On the other hand, since it is quite simple, some features are missing. An example of this is that there isn't possible remove a specific unit from an army.

##### Feedback

In this application, there is a lot the user can do wrong. The user can for example try to load a file that is not a CSV file, try to create a negative amonut of units or write text in the input field instead of a number. Giving feedback is therefore crucial to ensure the user doesn't break the program. When the user tries to do actions, they are not allowed to do, an error message pops up on the screen and tells the user why they can't do what they are trying to do.

##### Contrast

Text that is directly on the background picture has a white CSS background to make the contrast better. This makes it easier to read for everyone. The text would probably look prettier if the white background was a little transparrent, just as the lists are. The problem is that the text might be hart to read on certain image backrounds. Especially on the Forest background. The high contrast can make the text easier to read for visually impaired users. This ensures that the application has a more universal design.

##### Class diagram

![Class Diagram](source/images/ClassDiagram.png)

### Backend design patterns

##### Arrange-Act-Assert pattern

When creating tests, Arrange-Act-Assert was used. This means that first setup and initialization I made. Then actions required for the test. And lastly assert is used to verify the outcome of the test.

##### Factory pattern

The factory design pattern is a creational design pattern. Its purpose is to make it easier to create instances of units. In the Wargames application, this design pattern makes it a lot easier to create multiple units. One of the methods in the UnitFactory class is for creating multiple units of all types, while the other method is for creating a single specialized unit.

##### Observer pattern

The observer pattern is a behavioral design pattern. This Wargames application doesn't use the observer pattern a lot. In this app, it is only used to keep track of the state of the choicebox that contains the different terrains. This way, the terrain background is changed whenever a new terrain is chosen.

##### Coupling and cohesion

When creating this project, cohesion and coupling was kept in mind. Having loose coupling and high cohesion is crucial for having a good object-oriented design. The cohesion is high because every class has a specialized task. The coupling is quite loose as there aren’t any unnecessary dependencies between classes.

##### Don't repeat yourself

Duplicate code lines should most of the time be made into a method. In the Wargames application there are not many duplicate code lines. For example, the method updateCorrectList() in ArmyCreatorController.java is used instead of writing the content of the method many times. On the other hand, the method could be scrapped all together if the observable design patter had been used more.

## Implementation

This section will describe how the project is set up and structured.

The project is derived into packages for testing, resources, and the main application package. All the business logic is in the edu.ntnu.idatt2001 package. The edu.ntnu.idatt2001 therefore include the Model and Controller part of the MVC structure. The view files are in rescources. Below you can see the different packages in the file structure.

![File Structure](source/images/FileStructure.png)

##### pom.xml

The file pom.xml is responsible for installing dependencies and plugins into the project. Group and artifact id is also kept in the pom file. To identify the project, the group id edu.ntnu.idatt2001 was chosen. Java 17 is used as the Java version in this project. There are two different dependencies in the pom file. One of them is the JUnit dependency. This allows the project to have white box tests. The other dependeny is the javaFX dependency. This dependency allows the project to use JavaFX for creating the GUI. There are three plugins. One of them is a maven compiler plugin. Another one is the maven surefire plugin. The surefire plugin is used to execute tests. The last plugin is the JavaFX maven plugin. This plugin is needed for JavaFX to work in maven projects.

## Process

##### Version control

For local version control, Git was used. Git is very useful for commiting changes that were made to the project to a local or remote repository. Commit messages that was used were consise. Git is also useful for making branches. Before pushing the changes to GilLab, a branch was created. The branch had a name that described what it was trying to do. This way, when pushing to the remote repository, a merge request had to be made. Before the merge request could be merged, a pipeline had to run. The pipeline would check for any compiling or Git errors in the project.

GitLab was used as a remote repository. While working on the project, the issueboard on GitLab was not used. This is because it is not vital for a project of this size. All required tasks were awalible on Blackboard, which made the issue board less useful. When working on larger projects, the issueboard can be exceedingly useful.

##### Wireframe

Balsamiq Wireframes was used to create wireframes. The wireframes created were a scetch of how the GUI will look like. When designing the actual application, some additions were added for more funcitonality. 

![Wireframe](source/images/Wireframe.png)

## Reflection

This section contains my thoughts about what was done well, and what could have been improved upon while creating this project.

##### Improvements on cohesion

The cohesion in this project could have been a bit better if there was a BattleLog class. In the end, I didn't have enough time to implement this. If i had created a BattleLog class earlier, implementing a battle log would be a lot easier.

Another improvement would be to make a class for aleart boxes. This would make it eaiser to make more customized error messages. The cohesion would be higher if I hade done this, because now there is a method in both controllers for opening an error box.

##### Usage of Git and GitLab

While creating this project, I used Git and GilLab for version control. When doing the obligatory tasks that the students were handed, I often did only one git commit per task. Many of the tasks were too big for one commit. Commit messages are supposed to be small and concise. If many changes are made between each commit, the commit message becomes too long. When I was working on this project, I sometimes worked for longer periods of time without commiting. This led to a lot of changes being made with each commit. This made it hard to describe the changes made in a single commit message. In future projects I will commit more often. 

As stated before, I did not use the issueboard on GitLab. This is acceptable, but on the other hand, it could be useful to gather all my ideas about what to implement in one place.

##### Usage of backend design patterns

As mentioned before, the observable design pattern is only used once. In hindsight, it would maybe be easier to use the observable pattern to update the lists containing information about the state of the armies. This would make it so that the application can work without the methods that update the lists. The code would proberbly be a bit smaller with this change, but it was very easy to just implement methods that updates the lists.

##### Battle log

I am not satisfied about how to battle log turned out. The log contains all information about what happens in the fight, but it could have been made clearer. Right now, there is too much text, which makes it clunky to read.

##### What I have learned

During this project I have learned a lot about creating and managing a project. I am more aware of GUI design principles and design patterns. 

##### General improvements

If i were to start over, or had more time, I would probably have made the application more visually pleasing. Changeing the font and the apperance of the buttons would have made the application look a lot better. The default button, which is used in the GUI, look very outdated. Many more features could also have been added. Here are some examples: adding ways to remove units from an army, better overview of the armies in the main page and adding more terrains and units. I would also have used design patterns more frequent. Using the design patterns is not always easier. Singleton may make the code both harder to test and more inflexible. On the other hand, using the builder design pattern could be useful for making custom error dialogue boxes. 

## Conclusion

In the course idatt2001 programming 2, the students had to create a "Wargames" project with a GUI. There were some obligatory tasks that had to be done, but all features beyond that is up to the student. Git and GitLab was used as version control and remote repository. All base functionality was included in this project. Beyond that, an army creator along with a changing background was implemented. A wireframe was made to scetch the GUI before creating the GUI in Scene Builder. In the current state, the program works as intended. I could not find any bugs in testing that are not fixed. All in all, I am satisfied about how the project turned out. I have learned a lot and will defineatly be more prepared for both frontend and backend work in the future.


